#pragma once

#include <iostream>  // for std::cout
#include <stdexcept> // for std::runtime_error
#include <utility>   // for std::swap

template <typename K, typename D> class Dictionary {
  public:
    Dictionary() : head_(nullptr) {}

    const D &find(const K &key);              // to implement
    void insert(const K &key, const D &data); // to implement
    const D &remove(const K &key);            // to implement

    bool empty() const { return !head_; }

    void printInOrder() { _printInOrder(head_); };

    void clearTree() { remove(head_->key); }

    ~Dictionary() { clearTree(); }

  private:
    class TreeNode {
      public:
        const K &key;
        const D &data;

        TreeNode *left;
        TreeNode *right;

        TreeNode(const K &key, const D &data)
            : key(key), data(data), left(nullptr), right(nullptr) {}
    };

    TreeNode *head_;

    TreeNode *&_find(const K &key, TreeNode *&node) const;      // to implement
    const D &_remove(TreeNode *&node);                          // to implement
    TreeNode *&_iop_of(TreeNode *&cur) const;                   // to implement
    TreeNode *&_rightmost_of(TreeNode *&cur) const;             // to implement
    TreeNode *&_swap_nodes(TreeNode *&node1, TreeNode *&node2); // to implement

    void _printInOrder(TreeNode *node) {
        if (!node) {
            std::cout << " ";
            return;
        } else {
            _printInOrder(node->left);
            std::cout << "[" << node->key << " : " << node->data << "]";
            _printInOrder(node->right);
        }
    }
};

#include "Dictionary.hpp"
