#pragma once

#include "Dictionary.h"
#include <stdexcept>

template <typename K, typename D> const D &Dictionary<K, D>::find(const K &key) {
    TreeNode *&node = _find(key, head_);

    if (node == nullptr) {
        throw std::runtime_error("error: key not found");
    }

    return node->data;
}

template <typename K, typename D>
typename Dictionary<K, D>::TreeNode *&Dictionary<K, D>::_find(const K &key, TreeNode *&cur) const {
    if (cur == nullptr) {
        return cur;
    }

    else if (key == cur->key) {
        return cur;
    }

    else if (key < cur->key) {
        return _find(key, cur->left);
    }

    else {
        return _find(key, cur->right);
    }
}

template <typename K, typename D> void Dictionary<K, D>::insert(const K &key, const D &data) {
    TreeNode *&node = _find(key, head_);
    if (node) {
        throw std::runtime_error("error: insert() used on existing key");
    }
    node = new TreeNode(key, data);
}

template <typename K, typename D> const D &Dictionary<K, D>::remove(const K &key) {
    TreeNode *&node = _find(key, head_);
    if (!node) {
        throw std::runtime_error("error: remove() used on non-existent key");
    }
    return _remove(node);
}

template <typename K, typename D>
typename Dictionary<K, D>::TreeNode *&Dictionary<K, D>::_iop_of(TreeNode *&cur) const {
    if (!cur) {
        return cur;
    }

    if (!(cur->left)) {
        return cur->left;
    }

    return _rightmost_of(cur->left);
}

template <typename K, typename D>
typename Dictionary<K, D>::TreeNode *&Dictionary<K, D>::_rightmost_of(TreeNode *&cur) const {
    if (!cur) {
        return cur;
    }

    if (!(cur->right)) {
        return cur;
    }

    return _rightmost_of(cur->right);
}

template <typename K, typename D> const D &Dictionary<K, D>::_remove(TreeNode *&node) {
    if (!node) {
        throw std::runtime_error("error: _remove() used on non-existent key");
    }

    if (node->left == nullptr && node->right == nullptr) { // zero-child remove
        const D &data = node->data;
        delete node;
        node = nullptr;
        return data;
    }

    else if (node->left != nullptr && node->right == nullptr) { // one-child (left) remove
        const D &data = node->data;
        TreeNode *toDelete = node;
        node = node->left;
        delete toDelete;
        return data;
    }

    else if (node->left == nullptr && node->right != nullptr) { // one-child (right) remove
        const D &data = node->data;
        TreeNode *toDelete = node;
        node = node->right;
        delete toDelete;
        return data;
    }

    else { // two child remove
        TreeNode *&iop = _iop_of(node);

        if (!iop) {
            throw std::runtime_error("error in two-child remove: IOP not found");
        }

        TreeNode *&moved_node = _swap_nodes(node, iop);

        return _remove(moved_node);
    }
}

template <typename K, typename D>
typename Dictionary<K, D>::TreeNode *&Dictionary<K, D>::_swap_nodes(TreeNode *&node1,
                                                                    TreeNode *&node2) {

    TreeNode *orig_node1 = node1;
    TreeNode *orig_node2 = node2;

    if (node1->left == node2) {
        std::swap(node1->right, node2->right);
        node1->left = orig_node2->left;
        orig_node2->left = node1;
        node1 = orig_node2;
        return node1->left;

    } else if (node1->right == node2) {
        std::swap(node1->left, node2->left);
        node1->right = orig_node2->right;
        orig_node2->right = node1;
        node1 = orig_node2;
        return node1->right;

    } else if (node1 == node2->left) {
        std::swap(node1->right, node2->right);
        node2->left = orig_node1->left;
        orig_node1->left = node2;
        node2 = orig_node1;
        return node2->left;

    } else if (node1 == node2->right) {
        std::swap(node1->left, node2->left);
        node1->right = orig_node2->right;
        orig_node2->right = node1;
        node2 = orig_node1;
        return node2->right;

    } else {
        std::swap(node1->left, node2->left);
        std::swap(node1->right, node2->right);
        std::swap(node1, node2);
        return node2;
    }
}
