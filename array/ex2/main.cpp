#include <iostream>

int main() {
    int values[10] = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29};

    std::cout << sizeof(int) << std::endl;

    std::cout << "&values[2]: " << &values[2] << std::endl;
    std::cout << "(long)&values[2]: " << (long)&values[2] << std::endl;
    std::cout << "&values[0]: " << &values[0] << std::endl;
    std::cout << "(long)&values[0]: " << (long)&values[0] << std::endl;

    int offset = (long)&(values[2]) - (long)&(values[0]);
    std::cout << offset << std::endl;

    return 0;
}
