#include "../Cube.h"
#include <iostream>
#include <vector>

using uiuc::Cube;

int main() {
    std::vector<Cube> cubes{Cube(11), Cube(42), Cube(400)};

    std::cout << "Initial Capacity: " << cubes.capacity() << std::endl;
    std::cout << "Initial Size: " << cubes.size() << std::endl;

    cubes.push_back(Cube(800));
    std::cout << "After adding - Capacity: " << cubes.capacity() << std::endl;
    std::cout << "After adding - Size: " << cubes.size() << std::endl;

    int offset = (long)&(cubes[2]) - (long)&(cubes[0]);
    std::cout << "Memory separation: " << offset << std::endl;

    return 0;
}
